/**
 * 
 * com.ic2c.annoations<BR>
 * AuthWhileList.java<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月29日-下午2:00:19<BR> 
 * 2019
 */
package com.ic2c.annoations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 白名单验证
 * AuthWhileList<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月29日-下午2:00:19 <BR>
 * @version 1.0.0
 * 
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthWhiteList {
	public boolean value() default false;
}
