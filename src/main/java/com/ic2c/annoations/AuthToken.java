/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c.annoations <br>
 * TokenValid.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年3月30日-下午10:50:02 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c.annoations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 验证token是否过期  <br>
 * TokenValid <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年3月30日-下午10:50:02  <br>
 * @version 1.0.0
 * 
 */

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthToken {

	public String data() default "";
}
