/**
 * 易码当先 <br>
 * JpSpringBoot02 <br>
 * com.ic2c.web.user <br>
 * UserController.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2017年7月15日-下午9:22:47 <br>
 * 2017Allister-版权所有 <br>
 */
package com.ic2c.web.user;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import com.ic2c.bigdata.kafka.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.ic2c.bean.User;
import com.ic2c.dao.user.UserMapper;
import com.ic2c.util.JsonMapper;

/**
 *   <br>
 * UserController <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2017年7月15日-下午9:22:47  <br>
 * @version 1.0.0
 * 
 */
@RestController
public class UserController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Resource
	private UserMapper userMapper;
	
	/**
	 * 以post方式请求  查询用户集合
	 * com.ic2c.web.user <br>
	 * 方法名：findUserList<br>
	 * @author Allister.Liu(刘继鹏) <br>
	 * Email：laujip@163.com <br>
	 * 时间：2017年7月15日-下午10:02:09 <br>
	 * @param pageNo
	 * @param pageSize
	 * @return List<User><br>
	 * @exception <br>
	 * @since  1.0.0 <br>
	 */
	@RequestMapping(value = "/findUser/{pageNo}/{pageSize}")
	public List<User> findUserList(@PathVariable("pageNo")int pageNo, @PathVariable("pageSize")int pageSize){
		List<User> users = userMapper.queryUserList(pageNo, pageSize);
		return users;
	}
	
	
	@PostMapping("/queryUsers")
	public List<User> findUserLists(@RequestParam("pageNo")int pageNo, @RequestParam("pageSize")int pageSize){
		List<User> users = userMapper.queryUserList(pageNo, pageSize);
		return users;
	}
	
	
	@PostMapping("/qureyUserByAge")
	public String queryAgegt20ByAge(@Valid User user, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			String result = bindingResult.getFieldError().getDefaultMessage();
			LOGGER.error("表单验证出错了：" + result);
			return result;
		}
		JsonMapper jsonMapper = JsonMapper.nonDefaultMapper();
		List<User> users = userMapper.queryAgegt20ByAge(user.getAge());
		return jsonMapper.toJson(users);
	}


	@Resource
	private Producer producer;

	@GetMapping("/senKafkaMsg")
	public String senKafkaMsg(String topic, String msg){
		return producer.sendChannelMsg(topic, msg);
	}
}
