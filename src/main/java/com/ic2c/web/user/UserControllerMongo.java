/**
 * 
 * com.ic2c.web.user<BR>
 * UserControllerMongo.java<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月28日-上午10:48:06<BR> 
 * 2019
 */
package com.ic2c.web.user;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ic2c.bean.User;

/**
 * 
 * UserControllerMongo<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月28日-上午10:48:06 <BR>
 * @version 1.0.0
 * 
 */

@RestController
public class UserControllerMongo {
	
	@Resource
	private MongoTemplate mongoTemplate;
	
	
	
	@RequestMapping("addUserInfo")
	public void addUserInfo(){
		User user = new User();
		user.setUsername("Allister");
		user.setAge(16);
		user.setAddress("sdfaasfsa");
		mongoTemplate.insert(user);
		
	}
}
