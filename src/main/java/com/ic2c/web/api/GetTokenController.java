/**
 * 
 * com.ic2c.web.api<BR>
 * GetTokenController.java<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月29日-下午1:58:36<BR> 
 * 2019
 */
package com.ic2c.web.api;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ic2c.annoations.AuthWhiteList;

/**
 * 获取token
 * GetTokenController<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月29日-下午1:58:36 <BR>
 * @version 1.0.0
 * 
 */
@RestController
@RequestMapping("/api")
public class GetTokenController {
	
	
	@PostMapping("/getAccessToken/{apiKey}/{secret}")
	@AuthWhiteList
	public Map<String,Object> getAccessToken(@PathVariable("apiKey")String apiKey, @PathVariable("secret")String secret) {
		Map<String,Object> map = new HashMap<>();
		
		map.put("code", "0000");
		map.put("errMsg", "ok");
		map.put("access_token", UUID.randomUUID() + "" + UUID.randomUUID() + "");
		map.put("expires_in", "604800");
		
		
		return map;
	}
}
