/**
 * 易码当先 <br>
 * JpSpringBoot01 <br>
 * com.ic2c.web <br>
 * HelloController.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2017年7月15日-下午3:36:35 <br>
 * 2017Allister-版权所有 <br>
 */
package com.ic2c.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ic2c.annoations.AuthToken;
import com.ic2c.annoations.AuthUser;
import com.ic2c.bean.User;
import com.ic2c.exception.TokenException;
import com.ic2c.service.UserService;

/**
 *   <br>
 * HelloController <br>
 * @author Allister.Liu(刘继鹏) <br>
 * 时间：2017年7月15日-下午3:36:35  <br>
 * @version 1.0.0
 * 
 */
@RestController
public class HelloController {
	
	@Resource
	private UserService userService;
	
	//获取application.yml配置文件中的值
	@Value("${user.username}")
	private String username;
	
	@Value("${user.age}")
	private Integer age;
	
	@RequestMapping("/index")
	public String initIndex(){
		return "大家好，我叫 " + username + "， 今年+ " + age + "岁！！！";
	}
	
	
	@RequestMapping("/init/corsIdex")
	public List<User> corsGetUsers(@RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize){
		System.out.println("进来了。。。=================");
		List<User> users = userService.queryUserList(pageNo, pageSize);
		return users;
	}
	
	@AuthUser
	@RequestMapping("/init/corsGetindex")
	public String corsGetindex(){
		System.out.println("进来了。。。");
		return "success";
	}
	
	
	@AuthToken(data="auth")
	@RequestMapping("/tokenTest")
	public String tokenTest(String token){
		System.out.println("进来了。。。");
		return "success";
	}
	
	@AuthUser
	@RequestMapping("/tokenTest1/{token}")
	public Map<String, Object> tokenTest1(@PathVariable("token")String token){
		System.out.println("进来了。。。" + token);
		Map<String, Object> map = new HashMap<>();
		map.put("code", "8888");
		return map;
	}
	
	
	@AuthUser
	@RequestMapping("/tokenTest2")
	public Map<String, Object> tokenTest2(String token){
		System.out.println("进来了。。。" + token);
		Map<String, Object> map = new HashMap<>();
		map.put("code", "8888");
		return map;
	}
	
	
	@AuthUser
	@RequestMapping("/tokenTest3")
	public Map<String, Object> tokenTest3(String tokens){
		System.out.println("进来了。。。" + tokens);
		Map<String, Object> map = new HashMap<>();
		map.put("code", "8888");
		return map;
	}
	
	
//	public static void main(String[] args) {
//		int i = 0;
//		for(;;)
//		{
//			i++;
//			if(i > 200)
//				throw new TokenException("number is max 200.", "099");
//		}
//	}
	
}
