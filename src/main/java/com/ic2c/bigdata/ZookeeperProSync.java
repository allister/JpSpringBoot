/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c <br>
 * ZookeeperProSync.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年3月31日-下午1:25:44 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c.bigdata;

import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 源码分析：http://www.360doc.com/content/18/0705/21/43284313_768134193.shtml  <br>
 * ZookeeperProSync <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年3月31日-下午1:25:44  <br>
 * @version 1.0.0
 * 
 */
public class ZookeeperProSync implements Watcher {
	
	private static final Logger log = LoggerFactory.getLogger(ZookeeperProSync.class);
	
	private static CountDownLatch countDownLatch = new CountDownLatch(1);
	
	private static ZooKeeper zk = null;
	
	private static Stat stat = new Stat();

	
	public static void main(String[] args) {
		connZoo("10.10.1.14:2181,10.10.1.15:2181,10.10.1.16:2181", "/jpdb");
	}
	
	/**
	 * 
	 * com.ic2c <br>
	 * 方法名：connZoo<br>
	 * @author Allister.Liu(刘继鹏) <br>
	 * Email：laujip@163.com <br>
	 * 时间：2019年3月31日-下午1:36:31 <br>
	 * @param connStr	连接串
	 * @param path 数据存放目录<br>
	 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
	 * @exception <br>
	 * @since  1.0.0 <br>
	 */
	public static void connZoo(String connStr, String path) {
		try {
			// 连接zookeeper并注册一个默认的监听器
			zk = new ZooKeeper(connStr, 5000, new ZookeeperProSync());
			
			// 等待zk连接成功
			countDownLatch.await();
			
			// 获取path目录节点的配置数据，并注册默认的监听器
			log.info("data={}", new String(zk.getData(path, Boolean.TRUE, stat)));
			
			Thread.sleep(Integer.MAX_VALUE);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public void process(WatchedEvent event) {
		// zk连接成功的通知时间
		if(KeeperState.SyncConnected == event.getState())
		{
			if(EventType.None == event.getType() && null == event.getPath())
				countDownLatch.countDown();
			else if(EventType.NodeDataChanged == event.getType())
			{
				try {
					log.info("配置更新了，data={}", new String(zk.getData(event.getPath(), Boolean.TRUE, stat)));
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		
	}

}
