/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c.bigdata.kafka <br>
 * Producer.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月2日-下午10:34:22 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c.bigdata.kafka;

import javax.annotation.Resource;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

/**
 *   <br>
 * Producer <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月2日-下午10:34:22  <br>
 * @version 1.0.0
 * 
 */
@Component
public class Producer {

	@Resource
    private KafkaTemplate<String, Object> kafkaTemplate;


    public String sendChannelMsg(String toptic, String obj) {
        ListenableFuture<SendResult<String, Object>> future = kafkaTemplate.send(toptic, obj);
        RecordMetadata rmd = null;

        try {
            rmd = future.get().getRecordMetadata();
        } catch (Exception e){

        }

        return rmd.toString();
    }


}
