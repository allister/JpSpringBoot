/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c <br>
 * T1.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月22日-下午9:03:11 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;

import com.ic2c.bean.User;

/**
 *   <br>
 * T1 <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月22日-下午9:03:11  <br>
 * @version 1.0.0
 * 
 */
public class T1 {
	
	public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		getUserName();
	}

	
	public static void getUserName() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		User u = new User();
//		u.setId(1);
		u.setUsername("Allister");
		u.setAge(22);
		
		System.out.println(PropertyUtils.getProperty(u, "username"));
	}
}
