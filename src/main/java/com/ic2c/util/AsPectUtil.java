/**
 * 
 * com.ic2c.util<BR>
 * AsPectUtil.java<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月22日-上午9:21:03<BR> 
 * 2019
 */
package com.ic2c.util;

import java.util.HashMap;
import java.util.Map;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.CodeSignature;

/**
 * 
 * AsPectUtil<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月22日-上午9:21:03 <BR>
 * @version 1.0.0
 * 
 */
public class AsPectUtil {
	
	public static Map<String, Object> getNameAndValue(ProceedingJoinPoint joinPoint) {
        Map<String, Object> param = new HashMap<>();
 
        Object[] paramValues = joinPoint.getArgs();
        String[] paramNames = ((CodeSignature)joinPoint.getSignature()).getParameterNames();
 
        for (int i = 0; i < paramNames.length; i++) {
            param.put(paramNames[i], paramValues[i]);
        }
 
        return param;
    }
}
