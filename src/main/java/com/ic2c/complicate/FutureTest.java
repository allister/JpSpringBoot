/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c.complicate <br>
 * FutureTest.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月15日-下午9:56:12 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c.complicate;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *   <br>
 * FutureTest <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月15日-下午9:56:12  <br>
 * @version 1.0.0
 * 
 */
public class FutureTest {
	
	
	static class MyFuture implements Callable<String>{
		
		@Override
		public String call() throws Exception {
			System.out.println("MyFuture进来了....");
			Thread.sleep(2000);
			return "Done";
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newCachedThreadPool();
		System.out.println("0---------------");
		Future<String> future = executorService.submit(new MyFuture());
		System.out.println("1---------------");
		System.out.println(future.get());
		
		executorService.shutdown();
	}

}
