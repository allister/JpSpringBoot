/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c.core <br>
 * AuthTokenConfigerAdapter.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年3月30日-下午11:14:06 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ic2c.annoations.AuthToken;

/**
 *   <br>
 * AuthTokenConfigerAdapter <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年3月30日-下午11:14:06  <br>
 * @version 1.0.0
 * 
 */

@Configuration
public class AuthTokenInterceptor extends HandlerInterceptorAdapter{

	private static final Logger log = LoggerFactory.getLogger(AuthTokenInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
			HandlerMethod handlerMethod = (HandlerMethod) handler;
			
			AuthToken authToken = handlerMethod.getMethod().getAnnotation(AuthToken.class);
			if(null != authToken)
			{
				// token 验证
				log.info("token={}, data={}", request.getParameter("token"), authToken.data());
			}
			
		}
		
		return super.preHandle(request, response, handler);
	}
	
	
	
	
	

	
}
