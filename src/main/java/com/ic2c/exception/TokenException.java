/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c.exception <br>
 * TokenException.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月21日-下午9:44:36 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c.exception;

/**
 *   <br>
 * TokenException <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月21日-下午9:44:36  <br>
 * @version 1.0.0
 * 
 */
public class TokenException extends RuntimeException{
	
	private String code;

	
	public TokenException(String detailMessage, String code) {
		super(detailMessage);
		this.code = code;
	}
	
	

}
