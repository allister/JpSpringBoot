/**
 * 
 * com.ic2c.aspect<BR>
 * HttpAspect.java<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月18日-下午4:33:14<BR> 
 * 2019
 */
package com.ic2c.aspect;

import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * HttpAspect<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月18日-下午4:33:14 <BR>
 * @version 1.0.0
 * 
 */

@Aspect
@Component
@Slf4j
public class HttpAspect {
	
	@Pointcut("execution(* com.ic2c.web..*.*(..))")
	public void log (){}
	
	@Before("log()")
	public void before(JoinPoint joinPoint) {
		ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		
		// url
//		log.info("url = {}", request.getRequestURL());
//		// class.methed
//		log.info("class_methed = {}()", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
//		// ip
//		log.info("ip = {}", request.getRemoteAddr());
//		// args
//		log.info("args = {}", joinPoint.getArgs());
		
	}
	
	
	@After("log()")
	public void after() {
//		log.info("请求后....");
	}
	
	
	@AfterReturning(returning = "obj", pointcut = "log()")
	public void getReturnObj(Object obj) {
//		log.info("req = {}", obj.toString());
	}
}
