/**
 * 
 * com.ic2c.aspect<BR>
 * AuthWhileAop.java<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月29日-下午2:06:02<BR> 
 * 2019
 */
package com.ic2c.aspect;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.ic2c.util.redis.RedisDataUtil;

/**
 * 
 * AuthWhileAop<BR>
 * @author Allister.Liu(刘继鹏) <BR>
 * Date：2019年4月29日-下午2:06:02 <BR>
 * @version 1.0.0
 * 
 */

@Aspect
@Component
public class AuthWhiteAop {
	
	
	@Resource
	private RedisDataUtil redisDataUtil;
	
	
	@Pointcut("@annotation(com.ic2c.annoations.AuthWhiteList)")
	public void white(){}
	
	
	@Around("white()")
	public Object authWhite(ProceedingJoinPoint point) {
		
		
		try{
			
			Map<String,Object> map = null;
			Map<String, Object> params = getNameAndValue(point);
			if(null == params || !params.containsKey("apiKey") || !params.containsKey("secret"))
	        {
				map = new HashMap<>();
				map.put("code", 4001);
				map.put("errMsg", "params is error");
				return map;
	        }
			String secret = String.valueOf(params.get("secret"));
			String apiKey = String.valueOf(params.get("apiKey"));
			
			
			if(isEmpty(apiKey) || isEmpty(secret))
			{
				map = new HashMap<>();
				map.put("code", 4001);
				map.put("errMsg", "params is error");
				return map;
			}
			
			String sk = String.valueOf(redisDataUtil.get(apiKey));
			if(isEmpty(sk))
			{
				
				map = new HashMap<>();
				map.put("code", 4003);
				map.put("errMsg", "invalid_client");
				map.put("error_description", "Client authentication failed");
				return map;
			}
			
			if(sk.equals("hdahoifcsdn4324j2oi4u23o4hoi234hyoi"))
			{
				map = new HashMap<>();
				map.put("code", 4003);
				map.put("errMsg", "invalid_client");
				map.put("error_description", "unknown client id	");
				return map;
			}
    		
    		
			ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();
			HttpServletRequest request = attributes.getRequest();
			
			String ip = request.getRemoteAddr();
			
			if(StringUtils.isEmpty(String.valueOf(redisDataUtil.get(ip))))
			{
				map = new HashMap<>();
				map.put("code", 4002);
				map.put("errMsg", "Not on the white list");
				return map;
			}
			
			ip = null;
			
			return point.proceed();
		}catch(Throwable e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static boolean isEmpty(String data){
		if(null == data || "" == data.trim() || "null".equals(data))
			return true;
		return false;
	}
	
	
	public static Map<String, Object> getNameAndValue(ProceedingJoinPoint joinPoint) {
        Map<String, Object> param = new HashMap<>();
 
        Object[] paramValues = joinPoint.getArgs();
        String[] paramNames = ((CodeSignature)joinPoint.getSignature()).getParameterNames();
 
        for (int i = 0; i < paramNames.length; i++) {
            param.put(paramNames[i], paramValues[i]);
        }
 
        return param;
    }
}
