/**
 * 易码当先 <br>
 * JpSpringBoot <br>
 * com.ic2c.core <br>
 * AuthUserAop.java <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月21日-下午4:42:50 <br>
 * <a href="https://ic2c.cc">https://ic2c.cc</a> <br>
 * 2019 Allister-版权所有 <br>
 */
package com.ic2c.aspect;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ic2c.exception.TokenException;

/**
 * 注解失效：https://blog.csdn.net/fumushan/article/details/80090947  <br>
 * AuthUserAop <br>
 * @author Allister.Liu(刘继鹏) <br>
 * Email：laujip@163.com <br>
 * 时间：2019年4月21日-下午4:42:50  <br>
 * @version 1.0.0
 * 
 */

@Aspect
@Component
public class AuthUserAop {
	
	private static final Logger log = LoggerFactory.getLogger(AuthUserAop.class);
	
	@Pointcut("@annotation(com.ic2c.annoations.AuthUser)")
	public void auth() {}
	
	@Around("auth() && args(tokens)")
	public Object check(ProceedingJoinPoint point, String tokens) throws Throwable {
		System.out.println("#########" + tokens);
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> params = getNameAndValue(point);
        if(null == params || !params.containsKey("token"))
        {
        	map.put("code", "0003");
			map.put("msg", "参数错误");
			return map;
        }
        
        String token = (String) params.get("token");
        if(StringUtils.isEmpty(token))
        {
        	map.put("code", "0002");
			map.put("msg", "参数不能为空");
			return map;
        }
		if(!"Allister".equals(token))
		{
			map.put("code", "0001");
			map.put("msg", "未登录");
			return map;
		}
		
		return point.proceed();
		
			
	}
	
	
//	@Before(value = "auth() && args(token)")
//	public void check1(JoinPoint point, String token) throws Throwable {
//		
//		log.info("================={}", token);
//		Map<String, Object> map = new HashMap<>();
//		if(StringUtils.isEmpty(token))
//        {
//        	map.put("code", "0002");
//			map.put("msg", "参数不能为空");
//			throw new TokenException("token is exception", "0099");
//        }
//		if(!"Allister".equals(token))
//		{
//			map.put("code", "0001");
//			map.put("msg", "未登录");
//			throw new TokenException("token is exception", "0099");
//		}
//		
//			
//	}
	
	
	private Map<String, Object> getNameAndValue(ProceedingJoinPoint joinPoint) {
        Map<String, Object> param = new HashMap<>();
 
        Object[] paramValues = joinPoint.getArgs();
        String[] paramNames = ((CodeSignature)joinPoint.getSignature()).getParameterNames();
 
        for (int i = 0; i < paramNames.length; i++) {
            param.put(paramNames[i], paramValues[i]);
        }
 
        return param;
    }

}
